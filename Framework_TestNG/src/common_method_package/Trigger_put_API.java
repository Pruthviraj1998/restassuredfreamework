package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Put_Repository;

public class Trigger_put_API extends Put_Repository {

	public static int extract_statuscode(String requestBody, String URL) {
		int statuscode = given().header("Content-Type", "application/json").body(requestBody).when().put(URL).then()
				.extract().statusCode();
		return statuscode;

	}

	public static String responseBody(String requestBody, String URL) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(URL).then()
				.extract().response().asString();
		return responseBody;
	}
}
