package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Post_Request_Repository;

public class Trigger_API_Method extends Post_Request_Repository {

	public static int extract_Status_Code(String req_Body, String URL) {
		int StatusCode = given().header("Content-Type", "application/json").body(req_Body).when().post(URL).then()
				.extract().statusCode();
		return StatusCode;
	}

	public static String responseBody(String res_Body, String URL) {

		String ResponseBody = given().header("Content-Type", "application/json").body(res_Body).when().post(URL).then()
				.extract().response().asString();
		return ResponseBody;

	}
}