package common_utility_package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_logs {

	public static File Create_log_Directory(String DirectoryName) {
		String Current_Project_Directory = System.getProperty("user.dir");
		System.out.println(Current_Project_Directory);
		File Log_Directory = new File(Current_Project_Directory + "\\API_Logs\\" + DirectoryName);
		Delete_Directory(Log_Directory);
		Log_Directory.mkdir();
		return Log_Directory;
	}

	public static boolean Delete_Directory(File Directory) {
		boolean Directory_deleted = Directory.delete();
		if (Directory.exists()) {

			File[] files = Directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						Delete_Directory(file);
					} else {
						file.delete();
					}
				}
			}
			Directory_deleted = Directory.delete();
		}
		return Directory_deleted;
	}

	public static void evidence_creator(File dirname, String filename, String endpoint, String requestbody,
			String responsebody) throws IOException {
		// step .1 create file at location

		File newfile = new File(dirname + "\\" + filename + ".txt");
		System.out.println("new flie created to save evidence :" + newfile.getName());

		// step 2 write data into the file

		FileWriter datawriter = new FileWriter(newfile);

		datawriter.write("Endpoint :" + endpoint + "\n\n");
		datawriter.write("requestbody  : \n\n" + requestbody + "\n\n");
		datawriter.write("responseBody  : \n\n" + responsebody);
		datawriter.close();
		System.out.println("evidence is writtern in file" + newfile.getName());
	}

}
