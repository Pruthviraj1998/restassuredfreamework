Endpoint :https://reqres.in/api/users

requestbody  : 

{
    "name": "morpheus",
    "job": "leader"
}

responseBody  : 

{"name":"morpheus","job":"leader","id":"564","createdAt":"2023-10-19T06:33:02.441Z"}